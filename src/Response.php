<?php

declare(strict_types=1);

namespace Honeycombs\Request;

use Psr\Http\Message\ResponseInterface;

class Response extends \GuzzleHttp\Psr7\Response
{
    /**
     * {@inheritdoc}
     */
    public function sendHeaders(): ResponseInterface
    {
        foreach ($this->getHeaders() as $headerName => $header) {
            header($headerName . ': ' . $this->getHeaderLine($headerName), true);
        }

        return $this;
    }

    /**
     * Send response to client
     *
     * @return bool
     */
    public function send()
    {
        header('HTTP/' . $this->getProtocolVersion() . ' ' . $this->getStatusCode() . ' ' . $this->getReasonPhrase());
        $this->sendHeaders();
        echo $this->getBody()->getContents();

        return true;
    }
}
