<?php

declare(strict_types=1);

namespace Honeycombs\Request;

class Request extends \GuzzleHttp\Psr7\Request
{
    /**
     * Query params
     * @var array
     */
    private $query = [];

    /**
     * POST fields
     *
     * @var array
     */
    private $post;

    /**
     * {@inheritdoc}
     */
    public function __construct(string $method, $uri, array $headers = [], $body = null, string $version = '1.1')
    {
        parent::__construct($method, $uri, $headers, $body, $version);
        // Fill query by array of query parameters
        parse_str($this->getUri()->getQuery(), $this->query);
        $this->post = $_POST;
    }

    /**
     * Creating request instance using globals
     *
     * @return Request
     */
    public static function createByWebRequest()
    {
        $server = $_SERVER ?? [];
        [$_, $version] = explode('/', $server['SERVER_PROTOCOL'] ?? '/1.1');

        return new self(
            $server['REQUEST_METHOD'] ?? 'GET',
            $server['REQUEST_URI'] ?? '/',
            getallheaders(),
            file_get_contents('php://input'),
            $version
        );
    }

    /**
     * Gets query parameter
     *
     * @param string $variableName
     * @param null $default
     * @return mixed|null
     */
    public function getQueryParam(string $variableName, $default = null)
    {
        return array_key_exists($variableName, $this->query) ? $this->query[$variableName] : $default;
    }

    public function getPostParam(string $variableName, $default = null)
    {
        return array_key_exists($variableName, $this->post) ? $this->post[$variableName] : $default;
    }

    public function getServerParam(): void
    {
    }
}
