<?php

declare(strict_types=1);

namespace Tests\Honeycombs\Request;

use GuzzleHttp\Psr7\Uri;
use Honeycombs\Request\Request;
use PHPUnit\Framework\TestCase;

class RequestTest extends TestCase
{
    /**
     * Test creating request
     *
     * @return void
     *
     * @covers ::__construct
     */
    public function testConstruct(): void
    {
        $method = 'GET';
        $uri = '/';
        $headers = [];
        $body = 'a=1&b=2';
        $version = '1.1';

        $request = new Request(
            $method,
            $uri,
            $headers,
            $body,
            $version
        );

        $this->assertEquals($method, $request->getMethod());
        $this->assertEquals($uri, $request->getUri());
        $this->assertInstanceOf(Uri::class, $request->getUri());
        $this->assertEquals($headers, $request->getHeaders());
        $this->assertEquals($version, $request->getProtocolVersion());
    }

    /**
     * Test getting query parameter
     *
     * @return void
     *
     * @covers ::getQueryParam
     */
    public function testGetQueryParam(): void
    {
        $request = new Request('GET', '?some=value');
        $this->assertEquals('value', $request->getQueryParam('some'));
        $this->assertEquals('value', $request->getQueryParam('some', 'default'));
        $this->assertEquals('default', $request->getQueryParam('unexisting', 'default'));

        $request = new Request('GET', '?some[]=value');
        $this->assertEquals(['value'], $request->getQueryParam('some'));
        $this->assertEquals(['value'], $request->getQueryParam('some', 'default'));
        $this->assertEquals('default', $request->getQueryParam('unexisting', 'default'));

        $request = new Request('GET', '?some[]=value&some[]=value');
        $this->assertEquals(['value', 'value'], $request->getQueryParam('some'));
        $this->assertEquals(['value', 'value'], $request->getQueryParam('some', 'default'));
        $this->assertEquals('default', $request->getQueryParam('unexisting', 'default'));
    }

    /**
     * Test getting query parameter
     *
     * @return void
     *
     * @covers ::createByWebRequest
     */
    public function testCreateByWebRequest(): void
    {
        $headerName = 'some';
        $headerValue = 'val';
        $requestUri = '/foo/bar?a=b';
        $_SERVER['REQUEST_URI'] = $requestUri;
        $_SERVER['HTTP_' . strtoupper($headerName)] = $headerValue;
        $request = Request::createByWebRequest();
        $this->assertEquals([ucfirst($headerName) => [$headerValue]], $request->getHeaders());
        $this->assertEquals($requestUri, $request->getUri());
    }
}
